"""
    This is a minimalistic application that scrapes the temperature
    from the https://weerindelft.nl/ website.
    Exceptions catching sacrificed to readability.
"""

from requests_html import HTMLSession
import re, math, logging, sys


def get_html(url, object_id, delay=0):
    """
    :param url: string URL
    :param object_id: string id of any HTML object on website
    :param delay: delay before render return value
    :return: Element or None in case of failure
    """
    session = HTMLSession(browser_args=['--no-sandbox', '--user-agent=Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) '
                                                         'Gecko/20100101 Firefox/7.0.1'])
    session_get = session.get(url=url)
    session_get.html.render(sleep=delay)
    element = session_get.html.find('#{}'.format(object_id), first=True)
    session_get.close()
    return element


def math_rounding(n):
    """
    :param n: float number
    :return: integer (rounded value)
    """
    return int(math.floor(n + 0.5))


if __name__ == "__main__":

    url = "https://weerindelft.nl/"

    log = logging.getLogger(__name__)
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(logging.Formatter('%(levelname)s %(asctime)s: %(message)s'))
    log.addHandler(handler)
    log.setLevel(logging.INFO)

    iframe = get_html(url, "ifrm_3")
    if iframe and iframe.attrs.get('src'):
        span = get_html(iframe.attrs['src'], "ajaxtemp", 2)
        if span and span.attrs.get('lastobs'):
            string_temperature = re.search("\d+\.\d+", span.attrs['lastobs']).group()
            int_temperature = math_rounding(float(string_temperature))
            print("{} degrees Celsius".format(str(int_temperature)))
        else:
            log.error("Didn't get span with the temperature")
    else:
        log.error("Didn't get iframe url")


