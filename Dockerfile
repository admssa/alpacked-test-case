FROM python:3.7-slim-buster

RUN set -ex &&\
    apt-get update &&\
    apt-get install -yq --no-install-recommends \
    libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 \
    libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 \
    libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcursor1 libxdamage1 \
    libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 libnss3 &&\
    rm -rf /var/lib/apt/lists/* &&\
    pip install pipenv &&\
    useradd -ms /bin/bash appuser &&\
    mkdir -p /home/appuser/app

COPY Pipfile Pipfile
COPY Pipfile.lock Pipfile.lock

RUN set -ex &&\
    pipenv install --deploy --system

WORKDIR /home/appuser/app
USER appuser
RUN set -ex &&\
    python3 -c 'import pyppeteer; pyppeteer.chromium_downloader.download_chromium()'

COPY HoeWarmIsHetInDelft.py /home/appuser/app

ENTRYPOINT ["python3"]
CMD ["HoeWarmIsHetInDelft.py"]